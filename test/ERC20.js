
const {
    time,
    loadFixture,
} = require("@nomicfoundation/hardhat-network-helpers")
const { anyValue } = require("@nomicfoundation/hardhat-chai-matchers/withArgs")
const { expect } = require("chai")

describe("ERC20 contract test", function () {
    async function deployFixture() {
        const [owner, acc_1, acc_2, acc_3] = await ethers.getSigners()

        _name = 'A'
        _symbol = 'B'
        _decimals = 3

        const contract = await (await ethers.getContractFactory("ERC20"))
            .connect(owner)
            .deploy(_name, _symbol, _decimals)

        return { contract, owner, acc_1, acc_2, acc_3, _name, _symbol, _decimals }
    }
  
    describe("Check deployment", function () {
        it("- name", async function () {
            const { contract, _name } = await loadFixture(deployFixture)
            expect(await contract.name()).to.equal(_name)
        })
        it("- symbol", async function () {
            const { contract, _symbol } = await loadFixture(deployFixture)
            expect(await contract.symbol()).to.equal(_symbol)
        })
        it("- decimals", async function () {
            const { contract, _decimals } = await loadFixture(deployFixture)
            expect(await contract.decimals()).to.equal(_decimals)
        })
    })

    describe("Check mint", function () {
        it("- from owner", async function () {
            const { contract, owner, acc_1 } = await loadFixture(deployFixture)
            await expect(
                contract
                    .connect(owner)
                    .mint(acc_1.address, 3000)
            ).not.to.be.reverted
        })
        it("- from not an owner", async function () {
            const { contract, owner, acc_1 } = await loadFixture(deployFixture)
            await expect(
                contract
                    .connect(acc_1)
                    .mint(owner.address, 3000)
            ).to.be.revertedWith(
                "ERC20: You are not owner"
            )
        })
    })

    describe("Check transfer", function () {
        it("- enough tokens", async function () {
            const { contract, owner, acc_1, acc_2 } = await loadFixture(deployFixture)
            contract
                .connect(owner)
                .mint(acc_1.address, 3000)
            await expect(
                contract
                    .connect(acc_1)
                    .transfer(acc_2.address, 3000)
            ).not.to.be.reverted
        })
        it("- not enough tokens", async function () {
            const { contract, acc_1, acc_2 } = await loadFixture(deployFixture)
            await expect(
                contract
                    .connect(acc_1)
                    .transfer(acc_2.address, 3000)
            ).to.be.revertedWith(
                "ERC20: not enough tokens"
            )
        })
    })

    describe("Check transferFrom", function () {
        it("- not enough tokens", async function () {
            const { contract, owner, acc_1, acc_2, acc_3 } = await loadFixture(deployFixture)
            contract
                .connect(owner)
                .mint(acc_2.address, 2000)
            await expect(
                contract
                    .connect(acc_1)
                    .transferFrom(acc_2.address, acc_3.address, 3000)
            ).to.be.revertedWith(
                "ERC20: not enough tokens"
            )
        })
        it("- not approved", async function () {
            const { contract, owner, acc_1, acc_2, acc_3 } = await loadFixture(deployFixture)
            contract
                .connect(owner)
                .mint(acc_2.address, 3000)
            await expect(
                contract
                    .connect(acc_1)
                    .transferFrom(acc_2.address, acc_3.address, 3000)
            ).to.be.revertedWith(
                "ERC20: no permission to spend"
            )
        })
        it("- approved", async function () {
            const { contract, owner, acc_1, acc_2, acc_3 } = await loadFixture(deployFixture)
            contract
                .connect(owner)
                .mint(acc_2.address, 3000)
            contract
                .connect(acc_2)
                .approve(acc_1.address, 3000)
            await expect(
                contract
                    .connect(acc_1)
                    .transferFrom(acc_2.address, acc_3.address, 3000)
            ).not.to.be.reverted
        })
    })

    describe("Check other getters", function () {
        it("- balanceOf", async function () {
            const { contract, owner, acc_1, acc_2 } = await loadFixture(deployFixture)
            contract
                .connect(owner)
                .mint(acc_1.address, 2000)
            await expect(
                contract
                    .connect(acc_2)
                    .balanceOf(acc_1.address)
            ).not.to.be.reverted
        })
        it("- totalSupply", async function () {
            const { contract, owner, acc_1, acc_2, acc_3 } = await loadFixture(deployFixture)
            contract
                .connect(owner)
                .mint(acc_1.address, 2000)
            contract
                .connect(owner)
                .mint(acc_2.address, 3000)
            expect(
                await contract
                    .connect(acc_3)
                    .totalSupply()
            ).to.equal(
                5000
            )
        })
        it("- allowance", async function () {
            const { contract, owner, acc_1, acc_2, acc_3 } = await loadFixture(deployFixture)
            contract
                .connect(owner)
                .mint(acc_1.address, 3000)
            contract
                .connect(acc_1)
                .approve(acc_2.address, 2000)
            expect(
                await contract
                    .connect(acc_3)
                    .allowance(acc_1.address, acc_2.address)
            ).to.equal(
                2000
            )
        })
    })
  
    //   it("Should set the right owner", async function () {
    //     const { lock, owner } = await loadFixture(deployOneYearLockFixture);
  
    //     expect(await lock.owner()).to.equal(owner.address);
    //   });
  
    //   it("Should receive and store the funds to lock", async function () {
    //     const { lock, lockedAmount } = await loadFixture(
    //       deployOneYearLockFixture
    //     );
  
    //     expect(await ethers.provider.getBalance(lock.address)).to.equal(
    //       lockedAmount
    //     );
    //   });
  
    //   it("Should fail if the unlockTime is not in the future", async function () {
    //     // We don't use the fixture here because we want a different deployment
    //     const latestTime = await time.latest();
    //     const Lock = await ethers.getContractFactory("Lock");
    //     await expect(Lock.deploy(latestTime, { value: 1 })).to.be.revertedWith(
    //       "Unlock time should be in the future"
    //     );
    //   });
    

})
  