
main = async () => {
  const [owner, acc_1, acc_2] = await ethers.getSigners()

  const contract = await (await ethers.getContractFactory("ERC20"))
    .connect(owner)
    .deploy('A', 'B', 3)

  await contract
    .connect(owner)
    .mint(acc_1.address, 3000)

  await contract
    .connect(acc_1)
    .transfer(acc_2.address, 2000)
}

main()